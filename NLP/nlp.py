import spacy
from spacy import displacy
from spacy.util import minibatch, compounding
import random
import io

from spacy.gold import GoldParse
from spacy.scorer import Scorer

training_data = []

liste_ville = ['Paris','Nice','Marseille','Lyon','Lille']
proposition_depart = ["Je pars de ","Je prend un billet partant de ", "je choisis de partir de ", "je pars depuis ", "je commence mon voyage à ", "je veux un train de ", "Un aller simple de "]
proposition_arriver = [" pour aller à ", " et arrive à "," pour terminer à "," et aimerait aller à "]

for i in range(240):
    depart_index = random.randint(0, len(liste_ville)-1)
    arriver_index = -1
    while (depart_index == arriver_index | arriver_index == -1):
        arriver_index = random.randint(0, len(liste_ville)-1)

    depart = liste_ville[depart_index]
    arriver = liste_ville[arriver_index]

    proposition_depart_index = random.randint(0, len(proposition_depart)-1)
    proposition_arriver_index = random.randint(0, len(proposition_arriver)-1)

    phrase_depart = proposition_depart[proposition_depart_index] + depart
    phrase_arriver = proposition_arriver[proposition_arriver_index] + arriver

    training_data.append((phrase_depart+phrase_arriver,{"entities": [((len(phrase_depart)-len(depart), len(phrase_depart) ,"DEPART" )),((len(phrase_arriver) - len(arriver) + len(phrase_depart), len(phrase_arriver)+ len(phrase_depart), "ARRIVER"))]}))


def evaluate(ner_model, examples):

    scorer = Scorer()
    for input_, annot in examples:
        doc_gold_text = ner_model.make_doc(input_)
        gold = GoldParse(doc_gold_text, entities=annot.get('entities'))
        pred_value = ner_model(input_)
        scorer.score(pred_value, gold)
    return scorer.scores


lang = "fr"
pipeline = ["tagger", "parser"]

cls = spacy.util.get_lang_class(lang)
nlp = cls()                            
for name in pipeline:
    nlp.add_pipe(nlp.create_pipe(name)) 


ner = nlp.create_pipe("ner")
nlp.add_pipe(ner)
ner.add_label("DEPART")
ner.add_label("ARRIVER")

optimizer = nlp.begin_training()

sizes = compounding(1.0, 4.0, 1.001)
random.seed(0)
for itn in range(200):
    random.shuffle(training_data[0:200])
    batches = minibatch(training_data, size=sizes)
    losses = {}
    for batch in batches:
        texts, annotations = zip(*batch)
        nlp.update(texts, annotations, sgd=optimizer, drop=0.35, losses=losses)
    print("Losses", losses)

print(evaluate(nlp,training_data[200:]))

nlp.to_disk("./")