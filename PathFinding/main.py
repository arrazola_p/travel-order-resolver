import csv
from dijkstra import dijkstra
from graph import Graph
import sys
import getopt


def create_vertex_list(reader, g):
    mylist = []
    newlist = []
    for row in reader:
        trajet = row["trajet"].split(" - ")
        for i in trajet:
            mylist.append(i)
    mylist = list(dict.fromkeys(mylist))
    for i in mylist:
        if i not in newlist:
            g.add_vertex(i)


def create_edge_list(g, file):
    with open(file, mode='r') as f:
        reader = csv.DictReader(f, delimiter='\t')
        for row in reader:
            newtrajet = row["trajet"].split(" - ")
            g.add_edge(newtrajet[0], newtrajet[1], int(row["duree"]))


def shortest(v, path):
    if v.previous:
        path.append(v.previous.get_id())
        shortest(v.previous, path)
    return


def usage():
    print("Usage : ./main.py GARE_DE_DEPART GARE_ARRIVEE [OPTION...]\n")
    print("-h --help\taffiche cette aide")
    print("-f --file path\tutilise ce fichier")


if __name__ == '__main__':
    g = Graph()
    file = "timetables.csv"
    departure = 'Gare de Bourg-en-Bresse'
    arrival = 'Gare de Nevers'
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h:f", ["help", "file"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        print(opt, arg)
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-f", "--file"):
            file = arg

    if len(args) > 1:
        departure = args[0]
        arrival = args[1]

    with open(file, mode='r') as f:
        reader = csv.DictReader(f, delimiter='\t')
        create_vertex_list(reader, g)
        create_edge_list(g, file)
    dijkstra(g, g.get_vertex(departure))
    target = g.get_vertex(arrival)
    path = [target.get_id()]
    shortest(target, path)
    print('The shortest path : {}'.format(path[::-1]))
