# SpeechRecognition

#### *Requirement : python*

### In a terminal

    - pip install -r requirements.txt
    OR
    - pip install SpeechRecognition
    - pip install python-i18n
    - pip install inquirer
    - pip install pyaudio
    
    On Fedora, Mac or Python Virtualenv:
    If the python script don't parse correctly the YAML translate files, You may need: 
    - pip install PyYAML

### Run the script

    - python path/to_your_pythonFile.py 
        (example: python .\speechRec.py)

### **Troubleshooting (when install pyaudio) :**

**On Windows :**

    1) Visit this site: https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio

    2) Download the appropriate wheel corresponding to your Python Version

    3) Open Terminal and Install the provided wheel running the code below

    - pip install path/to_your_wheel.whl 
        (example : pip install .\PyAudio-0.2.11-cp37-cp37m-win_amd64.whl)

**On Fedora :**

    - dnf install python-devel
    - dnf install portaudio-devel
    - pip install pyaudio

**On Mac :**

    - brew install portaudio
    - pip install pyaudio
