import speech_recognition as sr
from os.path import abspath, dirname, join
import inquirer
import i18n

i18n.load_path.append(abspath(join(dirname(__file__), 'i18n')))

question = [
  inquirer.List('languageChoice', message="What language do you speak ?", choices=['fr', 'en'])
]
answer = inquirer.prompt(question)

i18n.set('locale', answer["languageChoice"])

r = sr.Recognizer()
try:
    with sr.Microphone() as source: # default Microphone is choosed (bonus: implement user choice)
        print(i18n.t('translate.listen'))
        audio = r.listen(source)

        try:
            text = r.recognize_google(audio, language=i18n.t('translate.audio_lang'))
            with open(file=abspath(join(dirname(__file__), 'voiceOrder.txt')), mode='w', encoding='utf8') as output:
                output.write(text)
            print(i18n.t('translate.saveFile'))

        except sr.UnknownValueError:
            print(i18n.t('translate.unknown_value_error'))
        except sr.RequestError as e:
            print(i18n.t('translate.request_error') + "; {0}".format(e))
        except:
            print(i18n.t('translate.global_error'))
except IOError:
    print(i18n.t('translate.audio_capture_error'))
except:
    print(i18n.t('translate.global_error'))
